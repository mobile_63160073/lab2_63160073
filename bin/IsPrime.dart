import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  print("Enter number :");
  int? n = int.parse(stdin.readLineSync()!);
  bool result = isPrime(n);
  if(result){
    print("Number $n is prime number");
  }else{
    print("Number $n is not prime number");
  }
}

bool isPrime(int number){
  if(number <= 1){
    return false;
  }
  
  for(int i = 2; i <= sqrt(number); i++){
    if(number%i == 0){
      return false;
    }
  }
  return true;
}
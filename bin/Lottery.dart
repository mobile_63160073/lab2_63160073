import 'dart:io';

void main(List<String> arguments) {
  print("Input your lottery number : ");
  String lottery = stdin.readLineSync()!;
  if(lottery.length != 6){
    print("lottery number must be six numbers");
  }
  checkLottery(lottery);
}

void checkLottery(String lottery,{bool reward: false}){
  if(firstPrize(lottery)){
    print("Wow!! You get a frist prize!!!!!!! 6,000,000 baht!!!!!!");
    reward = true;
  }
  if(secondPrize(lottery)){
    print("Wow!! You get a second prize!!!!!!! 200,000 baht!!!!!!");
    reward = true;
  }
  if(thirdPrize(lottery)){
    print("Wow!! You get a third prize!!!!!!! 80,000 baht!!!!!!");
    reward = true;
  }
  if(firstPrizeNeighbors(lottery)){
    print("Wow!! You get a frist prize neighbors!!!!!!! 100,000 baht!!!!!!");
    reward = true;
  }
  if(treeDigitPrefix(lottery)){
    print("Wow!! You get tree digit prefix!!!!!!! 4,000 baht!!!!!!");
    reward = true;
  }
  if(treeDigitSuffix(lottery)){
    print("Wow!! You get tree digit suffix!!!!!!! 4,000 baht!!!!!!");
    reward = true;
  }
  if(twoDigitSuffix(lottery)){
    print("Wow!! You get tree digit suffix!!!!!!! 2,000 baht!!!!!!");
    reward = true;
  }
  if(!reward){
    print("Sorry, You not get reward.");
  }
}

bool firstPrize(String lottery){
  String prize = '436594';
  if(lottery.contains(prize)){
    return true;
  }
  return false;
}

bool secondPrize(String lottery){
  var prize = ['502412','396501','084971','285563','049364'];
  for(int i = 0; i < prize.length; i++){
    if(lottery.contains(prize[i])){
      return true;
    }
  }
  return false;
}

bool thirdPrize(String lottery){
  var prize = ['662884','242496','575619','666926','422058','853019','896753','996939','166270','043691'];
  for(int i = 0; i < prize.length; i++){
    if(lottery.contains(prize[i])){
      return true;
    }
  }
  return false;
}

bool firstPrizeNeighbors(String lottery){
  var prize = ['436593','436595'];
  for(int i = 0; i < prize.length; i++){
    if(lottery.contains(prize[i])){
      return true;
    }
  }
  return false;
}

bool treeDigitPrefix(String lottery){
  var prize = ['266','893'];
  String lot = lottery.substring(0,4);
  for(int i = 0; i < prize.length; i++){
    if(lot.contains(prize[i])){
      return true;
    }
  }
  return false;
}

bool treeDigitSuffix(String lottery){
  var prize = ['447','282'];
  String lot = lottery.substring(3);
  for(int i = 0; i < prize.length; i++){
    if(lot.contains(prize[i])){
      return true;
    }
  }
  return false;
}

bool twoDigitSuffix(String lottery){
  String prize = '14';
  String lot = lottery.substring(4);
  if(lot.contains(prize)){
    return true;
  }
  return false;
}

